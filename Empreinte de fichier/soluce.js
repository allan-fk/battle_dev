const fs = require("fs");
const path = require("path");

const solve = (data, filename) => {
  if (filename.includes("output")) return;

  while (["111", "000"].every(e => data.indexOf(e) !== -1)) {
    data = data.replace("000", "00");
    data = data.replace("111", "1");
  }
  while (data.indexOf("10") !== -1) {
    data = data.replace("10", "1");
  }
  console.log("solve =>", data)
};

const readFiles = (dir, processFile) => {
  fs.readdir(dir, (error, fileNames) => {
    if (error) throw error;

    fileNames.forEach(filename => {
      const name = path.parse(filename).name;
      const filePath = path.resolve(dir, filename);

      const check = ["input", "output"].some(e => filename.includes(e + "4"));
      if (check)
        fs.readFile(filePath, "utf8", (err, data) => {
          if (error) throw error;
          solve(data, filename);
          processFile(name, data);
        });
    });
  });
};

readFiles(
  "sample-3r8-bDpk4oMKIhQu4AhzdH0L0MVDGsAlU2YXYGl0lSs",
  (name, data) => {
    console.log(name + " => " + data);
  }
);
