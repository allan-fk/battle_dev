const fs = require("fs");
const path = require("path");

const solve = (data, filename) => {
  if (filename.includes("output")) return;
  data = data
    .split("\n")
    .map(Number)
    .splice(1);

  let min = 0;
  let sum = 0;

  data.map(v => {
    sum + v < 0 ? (sum += v) : (sum = 0);
    min = Math.min(sum, min);
  });

  console.log("solve =>", min);
};

const readFiles = (dir, processFile) => {
  fs.readdir(dir, (error, fileNames) => {
    if (error) throw error;

    fileNames.forEach(filename => {
      const name = path.parse(filename).name;
      const filePath = path.resolve(dir, filename);

      const check = ["input", "output"].some(e => filename.includes(e + "5"));
      if (check)
        fs.readFile(filePath, "utf8", (err, data) => {
          if (error) throw error;
          solve(data, filename);
          processFile(name, data);
        });
    });
  });
};

readFiles(
  "sample-UyGomYq_-vQdgfp5hlKHuKgmVzP-yWFwEMnaESvH2Uc",
  (name, data) => {
    console.log(name + " => " + data);
  }
);
