var fs = require("fs");

fileRead = (dirpath) => {
  fs.readdir(dirpath, function(err, files) {
    const array = files.filter(el => /\.txt$/.test(el));
    array.map(filename => {
      fs.readFile(filename, "utf8", function(err, content) {
        if (err) console.log(err);
        console.log(content);
      });
    });
  });
};

fileRead("./");
